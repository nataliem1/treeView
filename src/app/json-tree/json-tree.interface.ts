export interface IJsonTreeNode {
    name: string;
    expanded?: boolean;
    children: any[];
}