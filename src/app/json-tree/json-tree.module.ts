import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TreeComponent } from './tree/tree.component';
import { SourceSelectComponent } from './source-select/source-select.component';
import { TreeViewShellComponent } from './tree-view-shell/tree-view-shell.component';

@NgModule({
  declarations: [TreeComponent, SourceSelectComponent, TreeViewShellComponent],
  imports: [
    CommonModule,
    HttpClientModule        
  ],
  exports: [TreeViewShellComponent]
})
export class JsonTreeModule { }
