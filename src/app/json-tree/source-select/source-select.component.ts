import { Component, OnInit } from '@angular/core';
import { GetJsonService } from '../get-json.service';

@Component({
  selector: 'app-source-select',
  templateUrl: './source-select.component.html',
  styleUrls: ['./source-select.component.scss']
})

export class SourceSelectComponent implements OnInit {
  sources: Array<object>;

  constructor(private jsonService: GetJsonService) { }

  ngOnInit(): void {
    this.sources = this.jsonService.getSources();
  }

  changeSource(url: string) {
    this.jsonService.setSource(url);
  }
}
