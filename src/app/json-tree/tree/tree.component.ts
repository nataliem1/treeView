import { Component, OnInit, Input } from '@angular/core';

import { IJsonTreeNode } from '../json-tree.interface';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})

export class TreeComponent implements OnInit {
  
  constructor() { }
  
  @Input() 
  jsonData: IJsonTreeNode[];
 
  ngOnInit(): void {
  }

  toggleExpand(node: IJsonTreeNode) {
    node.expanded = !node.expanded;
  }
}
