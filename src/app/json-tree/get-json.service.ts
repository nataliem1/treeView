import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';


//const _SOURCES = [ {name: 'Food', url: './assets/mock-data/food.json'}, {name: 'Family Tree', url: './assets/mock-data/family.json'}, {name: 'Music', url:'./assets/mock-data/music.json'}];
const _SOURCES = [ 
  {name: 'Food', url: 'https://extendsclass.com/api/json-storage/bin/fccebcf'}, 
  {name: 'Family Tree', url: 'https://extendsclass.com/api/json-storage/bin/ecefead'}, 
  {name: 'Music', url:'https://extendsclass.com/api/json-storage/bin/adebbba'}
];
@Injectable({
  providedIn: 'root'
})

export class GetJsonService {
  private dataSubject = new BehaviorSubject<object>({});
  
  constructor(private http: HttpClient) { }

  setSource(url: string | null) {
    if (url.indexOf('json') === -1) {
      this.dataSubject.next({});
    } else {
      this.http.get(url).subscribe( data => {
        this.dataSubject.next(data);
      });
    }  

  }

  getData(): Observable<any> {
    return this.dataSubject as Observable<any>;
  }

  getSources(): Array<object> {
    return _SOURCES;
  }
}
