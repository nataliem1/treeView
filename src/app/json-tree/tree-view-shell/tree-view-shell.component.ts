import { Component, OnInit } from '@angular/core';
import { GetJsonService } from '../get-json.service';
import { IJsonTreeNode } from '../json-tree.interface';

@Component({
  selector: 'app-tree-view-shell',
  templateUrl: './tree-view-shell.component.html',
  styleUrls: ['./tree-view-shell.component.scss']
})

export class TreeViewShellComponent implements OnInit {
  jsonData: IJsonTreeNode[] = [];
  
  constructor(private jsonService: GetJsonService) { }

  ngOnInit(): void {
    //subscribe to data change
    this.jsonService.getData().subscribe(data => {
      this.jsonData = (data && data.nodes) ? [...data.nodes] : [];
    })
  }

}
