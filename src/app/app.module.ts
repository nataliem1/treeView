import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { JsonTreeModule } from './json-tree/json-tree.module';


@NgModule({
  declarations: [
    AppComponent  
  ],
  imports: [
    BrowserModule, 
    JsonTreeModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
